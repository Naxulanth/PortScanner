#include "scanner.h"

// check whether a port is open or not, uses sfml network library 
bool check_port(const std::string & address, int port) { 

	bool check;
	sf::TcpSocket sock;
	check = (sock.connect(sf::IpAddress(address), port) == sf::Socket::Done);
	sock.disconnect();
	return check;
}