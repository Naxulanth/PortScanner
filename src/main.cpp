#include "scanner.h"
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <exception>
#include <sstream>

static bool term = false;
static bool con = false;
static bool state = true;
// address
static std::string s_add = "";
// port
static std::string s_port = "";

std::string open(int port) {
	return "Port " + std::to_string(port) + ": Open";
}

std::string closed(int port) {
	return "Port " + std::to_string(port) + ": Closed";
}

void scan_ports(std::string add, std::string port) {
	s_add = add;
	s_port = port;
	// check for invalid input characters
	bool invalid = false;
	// check if warning message is displayed before
	bool warn = false;
	// range of ports or singular port
	bool range = false;
	// position of dash to pull bounds from string
	int pos = 0;
	// output file stream for results
	std::ofstream results;
	// temporary
	std::string temp = "";
	// temporary int
	int i_temp = 0;
	// lower bound
	int port1 = 0;
	// upper bound
	int port2 = 0;
	do { 
		invalid = false;
		for (int i = 0; i < port.size(); ++i) {
			// check for invalid characters/formatting in port input
			if (
				(
				((int)port[i] == 45)
					&&
					((i != 0) || (i != port.size() - 1))
					&&
					(((int)port[i - 1] >= 48) && ((int)port[i - 1] <= 57))
					&&
					(((int)port[i + 1] >= 48) && ((int)port[i + 1] <= 57))
					)
				|| ((int)port[i] == 44)
				|| (((int)port[i] >= 48) && ((int)port[i] <= 57))
				) {
				continue;
			}
			else {
				if (con) {
					term = true;
				}
				else {
					invalid = true;
				}
				break;
			}
		}
		if (term) {
			std::cout << "Invalid input! Terminating." << std::endl;
			break;
		}
		if (invalid) {
			std::cout << "Enter Port(s) ex. 80,50,250-300 (inclusive) \n" << std::endl;
			// port
			std::cin >> port;
		}
	} while (invalid);
	// convert input into stringstream
	std::istringstream port_stream(port);
	// create "results.txt" and open handle to write results
	results.open("results.txt");
	// tokenize stringstream with commas
	while ( (std::getline(port_stream, temp, ',')) && (!(term)) ) {
		// check if the given port is a range or a single port
		for (int i = 0; i < temp.size(); ++i) {
			if (temp[i] == '-') {
				if (range && (!warn)) {
					std::cout << "Warning! Having more than one dash in a range is invalid! Only the first will be taken into account. \n" << std::endl;
					warn = true;
				}
				range = true;
				// position of dash to pull lower and upper range from the string later
				if (!(warn)) pos = i;

			}
		}
		try {
			// if it's a range, set the bounds
			if (range) {
				port1 = std::stoi(temp.substr(0, pos));
				port2 = std::stoi(temp.substr((pos + 1), temp.size()));
				// lower bound cant be bigger than upper bound
				if (port1 > port2) {
					std::swap(port1, port2);
					std::cout << "Warning! Lower bound can't be bigger than upper bound, values are swapped. \n" << std::endl;
				}
				// push into mandatory port ranges if they are improper ports
				if (port1 < 0) port1 = 1;
				if (port1 > 65535) port1 = 65535;
				if (port2 < 0) port2 = 1;
				if (port2 > 65535) port2 = 65535;
				// scan ports in the range
				for (int i = port1; i < (port2 + 1); ++i) {
					std::cout << "Scanning port:" + std::to_string(i) << std::endl;
					if (check_port(add, i)) {
						// display & write into file
						std::cout << open(i) << std::endl;
						results << open(i) + "\n";
					}
				}
			}
			// if it's not a range, scan for the individual port
			else {
				i_temp = std::stoi(temp);
				std::cout << "Scanning port:" + std::to_string(i_temp) << std::endl;
				if (check_port(add, i_temp)) {
					std::cout << open(i_temp) << std::endl;
					results << open(i_temp) + "\n";
				}
			}
			// reset values for fresh iteration of loop
			warn = false;
			temp = "";
			range = false;
		}
		// catch any exceptions which could occur, state reason & restart the program
		catch (std::exception& e) {
			std::cout << e.what() << '\n';
			state = true;
		}
	}
	results.close();
}

int main(int argc, char* argv[]) {
	if (argc == 3) {
		con = true;
		scan_ports(argv[1], argv[2]);
	}
	else {
		while (state) {
			state = false;
			std::cout << " \nPort Scanner" << std::endl;
			std::cout << "" << std::endl;
			std::cout << "Enter IP" << std::endl;
			// address
			std::cin >> s_add;
			std::cout << "Enter Port(s) ex. 80,50,250-300 (inclusive) " << std::endl;
			// port
			std::cin >> s_port;
			scan_ports(s_add, s_port);
		}
	}
	system("pause");
	if (term) return 1;
	else return 0;
}