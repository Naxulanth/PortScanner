# Port Scanner
Simple port scanner written in C++ using [SFML] Network library.

# Input 
Input IP Address and port(s) to be scanned.

For instance:

    localhost
    80,442-444

It is also possible to send input via command-line without using the interactive menu.
	
	portscanner localhost 80,442-444

This should scan ports 80, 442 ,443, 444 on localhost.
  
 Output is shown both in console, and written into a text file. (results.txt)


   [sfml]: https://www.sfml-dev.org/index.php